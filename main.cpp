#include <iostream>
#include <vector>
#include "Engine.h"
#include "TestStand.h"

int main() {
    int I = 10;

    std::vector<int> M = {20, 75, 100, 105, 75, 0};
    std::vector<int> V = {0, 75, 150, 200, 250, 300};

    double Hm = 0.01;
    double Hv = 0.0001;
    double C = 0.1;

    double T = 110;
    double Tenv;

    std::cout << "Enter the temperature of environment: ";
    std::cin >> Tenv;

    Engine Ural = Engine(I, M, V, T, Hm, Hv, C); //For example
    TestStand Stand = TestStand(Ural, Tenv);

    double maxEps = 0.0000001;
    int maxTime = 18000;

    int time = Stand.startEngine(maxEps, maxTime);
    if (time == maxTime)
    {
        std::cout << "For this environment temperature the engine is hasn't overheat." << std::endl;
    }
    else
    {
        std::cout << "Overheating time: " << time << " seconds" << std::endl;
    }

    return 0;
}
