#include "functions.h"

double getAcceleration(double M, int I)
{
    return I != 0 ? M / I : 0;
}

double getEngineHeatSpeed(double M, double Hm, double V, double Hv)
{
    return M * Hm + V * V * Hv;
}

double getEngineCoolingSpeed(double C, double Tenv, double Teng)
{
    return C * (Tenv - Teng);
}
