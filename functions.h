#pragma once
#ifndef ENGINE_FUNCTIONS_H
#define ENGINE_FUNCTIONS_H

#include "Engine.h"

/**
 * Get acceleration (a)
 *
 * @param double M - Torque
 * @param int I    - Moment of inertia
 *
 * @return float
 * */
double getAcceleration(double M, int I);

/**
 * Get speed heat of engine (Vh)
 *
 * @param double M    - Torque
 * @param double V    - Rotational speed
 * @param double Hm   - The coefficient of dependence of the heating rate on torque
 * @param double Hv   - The coefficient of dependence of the heating rate on the rotational speed of the crankshaft
 *
 * @return float
 * */
double getEngineHeatSpeed(double M, double Hm, double V, double Hv);

/**
 * Get speed cool of engine (Vc)
 *
 * @param double Tenv - Temperature of environment
 * @param double Teng - Temperature of engine
 * @param double C    - Coefficient of dependence of the cooling rate on engine temperature and the environment
 *
 * @return float
 * */
double getEngineCoolingSpeed(double C, double Tenv, double Teng);

#endif
