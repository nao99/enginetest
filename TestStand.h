#ifndef ENGINE_TESTSTAND_H
#define ENGINE_TESTSTAND_H

#pragma once
#include "Engine.h"

class TestStand
{
public:
    /**
     * Temperature of environment
     *
     * @var double T
     * */
    double Tenv;

    /**
     * Superheat temperature
     *
     * @var double T
     * */
    double Teng;

    /**
     * Engine
     *
     * @var Engine engine
     * */
    Engine *engine;

    /**
     * Constructor
     *
     * @param Engine &engine
     * @param double Tenv
     * */
    TestStand(Engine &engine, double Tenv);

    /**
     * Start engine method
     *
     * @param double maxEps
     * @param int    maxTime
     *
     * @return int
     * */
    int startEngine(double maxEps, int maxTime);
};

#endif
