#include "Engine.h"

Engine::Engine(int I, const std::vector<int> &M, const std::vector<int> &V, double T, double Hm, double Hv, double C)
{
    this->I = I;
    this->T = T;
    this->M = M;
    this->V = V;

    this->Hm = Hm;
    this->Hv = Hv;
    this->C = C;
}
