#include "TestStand.h"
#include "functions.h"

TestStand::TestStand(Engine &engine, double Tenv)
{
    this->engine = &engine;
    this->Teng = Tenv;
    this->Tenv = Tenv;
}

int TestStand::startEngine(double maxEps, int maxTime)
{
    unsigned int index = 0;

    double eps = this->engine->T - this->Teng;
    double currentV = this->engine->V[index];
    double currentM = this->engine->M[index];
    double a = getAcceleration(currentM, this->engine->I);

    int seconds = 0;

    while (eps > maxEps && seconds < maxTime)
    {
        seconds++;
        currentV += a;

        if (index < this->engine->M.size() - 2)
        {
            if (currentV >= this->engine->V[index + 1])
            {
                index += 1;
            }
        }

        double increase = currentV - this->engine->V[index];
        double reduction = this->engine->V[index + 1] - this->engine->V[index];
        double F = this->engine->M[index + 1] - this->engine->M[index];

        currentM += increase / F * reduction + this->engine->M[index];

        this->Teng += getEngineCoolingSpeed(this->engine->C, this->Teng, this->Tenv) + getEngineHeatSpeed(currentM, this->engine->Hm, currentV, this->engine->Hv);

        a = getAcceleration(currentM, this->engine->I);
        eps = this->engine->T - this->Teng;
    }

    return seconds;
}
