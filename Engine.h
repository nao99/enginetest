#ifndef ENGINE_ENGINE_H
#define ENGINE_ENGINE_H

#pragma once
#include <vector>

class Engine
{
public:
    /**
     * Moment of inertia in kg * m^2
     *
     * @var int I
     * */
    int I;

    /**
     * Torque in N * m
     *
     * @var vector int M
     * */
    std::vector<int> M;

    /**
     * Rotational speed in rad / s
     *
     * @var vector int *V
     * */
    std::vector<int> V;

    /**
     * Superheat temperature in celsius
     *
     * @var double T
     * */
    double T;

    /**
     * The coefficient of dependence of the heating rate on torque in T / (N * m * s)
     *
     * @var double Hm
     * */
    double Hm;  //

    /**
     * The coefficient of dependence of the heating rate on the rotational speed of the crankshaft in (T * s) / rad^2
     *
     * @var double Hv
     * */
    double Hv;

    /**
     * The coefficient of dependence of the cooling rate on engine temperature and the environment in 1 / s
     *
     * @var double C
     * */
    double C;

    /**
     * Constructor
     *
     * @param int I
     * @param vector int &M
     * @param vector int &V
     * @param double T
     * @param double Hm
     * @param double Hv
     * @param double C
     * */
    Engine(int I, const std::vector<int> &M, const std::vector<int> &V, double T, double Hm, double Hv, double C);
};

#endif
